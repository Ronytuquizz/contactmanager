﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ContactManager
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "vcf files (*.vcf)|*.vcf|All files (*.*)|*.*";
            openFileDialog1.ShowDialog();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            textBox1.Text = openFileDialog1.FileName;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            string text = System.IO.File.ReadAllText(textBox1.Text);
            string[] data= text.Replace("@", "$").Replace("\r\n", "@").Split('@');
            foreach (string item in data)
            {
                analizar(item);
            }
        }

        public string GetUntilOrEmpty(string text, string stopAt = ":")
        {
            if (!String.IsNullOrWhiteSpace(text))
            {
                int charLocation = text.IndexOf(stopAt, StringComparison.Ordinal);

                if (charLocation > 0)
                {
                    return text.Substring(0, charLocation);
                }
            }

            return String.Empty;
        }

        private void analizar(string item)
        {
            try
            {
                string comando = GetUntilOrEmpty(item).ToLower();
                switch (comando)
                {
                    case "begin":
                        p("Inicializando lectura del archivo");
                        break;
                    default:
                        break;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void p(string v)
        {
            Console.WriteLine(v);
        }
    }
}
